const fs = require('fs');
const path = require('path');

const Canvas = require('canvas'); // 引入 node canvas

// 引入 F2: start
const F2 = require('@antv/f2/lib/core'); // 引入核心包
require('@antv/f2/lib/geom/'); // 几何标记对象
require('@antv/f2/lib/geom/adjust/'); // 数据调整
require('@antv/f2/lib/coord/polar'); // 极坐标系
require('@antv/f2/lib/component/axis/circle'); // 极坐标系下的弧长坐标轴
require('@antv/f2/lib/scale/time-cat'); // timeCat 类型的度量
require('@antv/f2/lib/component/guide'); // 加载 guide 组件
const Guide = require('@antv/f2/lib/plugin/guide'); // Guide 插件
const Legend = require('@antv/f2/lib/plugin/legend'); // Legend 插件
F2.Chart.plugins.register([Legend, Guide]); // 注册以上插件
// 引入 F2: end

const canvas = Canvas.createCanvas(546, 576); // 创建 canvas 对象

// 使用 F2 绘制饼图
function drawPie(query) {
  // const data = [
  //   { name: '芳华', percent: 0.4, a: '1' },
  //   { name: '妖猫传', percent: 0.2, a: '1' },
  //   { name: '机器之血', percent: 0.18, a: '1' },
  //   { name: '心理罪', percent: 0.15, a: '1' },
  //   { name: '寻梦环游记', percent: 0.05, a: '1' },
  //   { name: '其他', percent: 0.02, a: '1' },
  // ];
  // const chart = new F2.Chart({
  //   el: canvas,
  //   width: 375,
  //   height: 260,
  //   padding: [45, 'auto', 'auto'],
  // });
  // chart.source(data, {
  //   percent: {
  //     formatter(val) {
  //       return val * 100 + '%';
  //     },
  //   },
  // });
  // chart.legend({
  //   position: 'right',
  // });
  // chart.coord('polar', {
  //   transposed: true,
  //   radius: 0.85,
  // });
  // chart.axis(false);
  // chart
  //   .interval()
  //   .position('a*percent')
  //   .color('name', [
  //     '#1890FF',
  //     '#13C2C2',
  //     '#2FC25B',
  //     '#FACC14',
  //     '#F04864',
  //     '#8543E0',
  //   ])
  //   .adjust('stack')
  //   .style({
  //     lineWidth: 1,
  //     stroke: '#fff',
  //     lineJoin: 'round',
  //     lineCap: 'round',
  //   });
    // function getFormattedDate(ts) {
    //     var date = new Date(ts);

    //     var month = date.getMonth() + 1;
    //     var day = date.getDate();
    //     var hour = date.getHours();
    //     var min = date.getMinutes();
    //     var sec = date.getSeconds();

    //     month = (month < 10 ? "0" : "") + month;
    //     day = (day < 10 ? "0" : "") + day;
    //     hour = (hour < 10 ? "0" : "") + hour;
    //     min = (min < 10 ? "0" : "") + min;
    //     sec = (sec < 10 ? "0" : "") + sec;

    //     var str = date.getFullYear() + "-" + month + "-" + day + " " +  hour + ":" + min + ":" + sec;

    //     /*alert(str);*/

    //     return str;
    // }

    let data = query && query.data
    let type = query && query.type || "5M"
    data = decodeURIComponent(decodeURIComponent(data))
    data = JSON.parse(data)
    let height = query && query.height || 576
    let width = query && query.width || 546

    var chart = new F2.Chart({
      el: canvas,
      margin:0,
      width:width,
      height:height,
    });



    chart.source(data, {
      time: {
        type: 'cat',
        tickCount: 6,
        formatter:function(txt){
          console.log(txt)
          if(type == "5M") {
            let yymmdd = txt.split(" ")[1]
            return yymmdd.substr(0,5)
          } 
          if(type == "1H") {
            let yymmdd = txt.split(" ")[1]
            return yymmdd.substr(0,5)
          } 
          if(type == "1D") {
            let yymmdd = txt.split(" ")[0]
            return yymmdd.substr(5).replace("-","/")
          } 
        },
        range: [0, 1]
      },
      tem: {
        tickCount: 5,
        formatter:function(txt){
          console.log(txt)
          // if(type === "yield") {
          //   return "$" + txt
          // } else {
          //   return (txt*100).toFixed(2) + "%"
          // }
          return txt.toFixed(2)
        },

      }
    });
    chart.axis('tem', {
      position:"right",
      grid:null,
      label: {
        textAlign: 'left', // 文本对齐方向，可取值为： left center right
        fill: '#434B5F', // 文本的颜色
        fontSize: '15', // 文本大小
        fontWeight: 'bold', // 文本粗细
        fontFamily: "Arial",
      },
      labelOffset:20
    });
    chart.axis('time', {
      label: {
        textAlign: 'center', // 文本对齐方向，可取值为： left center right
        fill: '#8E939F', // 文本的颜色
        fontSize: '15', // 文本大小
        fontFamily: "Arial",
      },

      label: (text, index, total) => {
        const cfg = {
          textAlign: 'start', // 文本对齐方向，可取值为： left center right
          fill: '#8E939F', // 文本的颜色
          fontSize: '15', // 文本大小
          fontFamily: "Arial",
        }
        // 第一个点左对齐，最后一个点右对齐，其余居中，只有一个点时左对齐
        if (index === 0) {
          cfg.textAlign = 'start';
        }
        if (index > 0 && index === total - 1) {
          cfg.textAlign = 'end';
        }
        // cfg.text = text + '%'; // cfg.text 支持文本格式化处理
        return cfg;
      },

      labelOffset:50,
      line:null,



    });
    // chart.tooltip({
    //   showCrosshairs: true,
    //   custom: true,
    //   showXTip: true,
    //   showYTip: true,
    //   snap: true,
    //   tooltipMarkerStyle: {
    //     fill: '#fff', // 设置 tooltipMarker 的样式
    //     lineWidth: 3,
    //     stroke: '#6F40EE',
    //   },
    //   crosshairsType: 'xy',
    //   crosshairsStyle: {
    //     lineDash: [2]
    //   }



    // });


    chart.line().position('time*tem').shape('smooth').size(1).color('#6F40EE');
    chart.area({startOnZero:false}).position('time*tem').shape('smooth').color("l(90) 0:#6F40EE 0.5:#DBD0FB 1:#fff").style({
      fillOpacity: 0.2,
    });

    chart.render();


}

// canvas
//   .createPNGStream()
//   .pipe(fs.createWriteStream(path.join(__dirname, 'pie.png'))); // 导出图片


const express = require('express')
const app = express()
const port = 3002
var md5 = require('md5');

app.get('/', (req, res) => {
  console.log(req.query)
  let query = req.query
  drawPie(query);

  let filename = md5(query.data)
  canvas
    .createPNGStream()
    .pipe(fs.createWriteStream(path.join(__dirname, './chart_' + filename + '.png')).on('finish', () =>  
      res.sendFile(path.join(__dirname, './chart_' + filename + '.png'))
    )); // 导出图片
  

})

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))


