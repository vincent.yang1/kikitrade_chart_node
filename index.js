var getRawBody = require('raw-body');
var getFormBody = require('body/form');
var body = require('body');

const fs = require('fs');
const path = require('path');
var md5 = require('md5');

const Canvas = require('canvas'); // 引入 node canvas

// 引入 F2: start
const F2 = require('@antv/f2/lib/core'); // 引入核心包
require('@antv/f2/lib/geom/'); // 几何标记对象
require('@antv/f2/lib/geom/adjust/'); // 数据调整
require('@antv/f2/lib/coord/polar'); // 极坐标系
require('@antv/f2/lib/component/axis/circle'); // 极坐标系下的弧长坐标轴
require('@antv/f2/lib/scale/time-cat'); // timeCat 类型的度量
require('@antv/f2/lib/component/guide'); // 加载 guide 组件
const Guide = require('@antv/f2/lib/plugin/guide'); // Guide 插件
const Legend = require('@antv/f2/lib/plugin/legend'); // Legend 插件
F2.Chart.plugins.register([Legend, Guide]); // 注册以上插件
// 引入 F2: end

const canvas = Canvas.createCanvas(375, 260); // 创建 canvas 对象

// 使用 F2 绘制饼图
function drawPie(query) {


    console.log(query)
    console.log(query.data)
    let data = query && query.data
    let type = query && query.type || "5M"
    data = decodeURIComponent(decodeURIComponent(data))
    data = JSON.parse(data)
    let height = query && query.height || 576
    let width = query && query.width || 546

    var chart = new F2.Chart({
      el: canvas,
      margin:0,
      width:width,
      height:height,
    });





    chart.source(data, {
      time: {
        type: 'cat',
        tickCount: 6,
        formatter:function(txt){
          console.log(txt)
          if(type == "5M") {
            let yymmdd = txt.split(" ")[1]
            return yymmdd.substr(0,5)
          } 
          if(type == "1H") {
            let yymmdd = txt.split(" ")[1]
            return yymmdd.substr(0,5)
          } 
          if(type == "1D") {
            let yymmdd = txt.split(" ")[0]
            return yymmdd.substr(5).replace("-","/")
          } 
        },
        range: [0, 1]
      },
      tem: {
        tickCount: 5,
        formatter:function(txt){
          console.log(txt)

          return txt.toFixed(2)
        },

      }
    });
    chart.axis('tem', {
      position:"right",
      grid:null,
      label: {
        textAlign: 'left', // 文本对齐方向，可取值为： left center right
        fill: '#434B5F', // 文本的颜色
        fontSize: '15', // 文本大小
        fontWeight: 'bold', // 文本粗细
        fontFamily: "Arial",
      },
      labelOffset:20
    });
    chart.axis('time', {
      label: {
        textAlign: 'center', // 文本对齐方向，可取值为： left center right
        fill: '#8E939F', // 文本的颜色
        fontSize: '15', // 文本大小
        fontFamily: "Arial",
      },

      label: (text, index, total) => {
        const cfg = {
          textAlign: 'start', // 文本对齐方向，可取值为： left center right
          fill: '#8E939F', // 文本的颜色
          fontSize: '15', // 文本大小
          fontFamily: "Arial",
        }
        // 第一个点左对齐，最后一个点右对齐，其余居中，只有一个点时左对齐
        if (index === 0) {
          cfg.textAlign = 'start';
        }
        if (index > 0 && index === total - 1) {
          cfg.textAlign = 'end';
        }
        // cfg.text = text + '%'; // cfg.text 支持文本格式化处理
        return cfg;
      },

      labelOffset:50,
      line:null,



    });



    chart.line().position('time*tem').shape('smooth').size(1).color('#6F40EE');
    chart.area({startOnZero:false}).position('time*tem').shape('smooth').color("l(90) 0:#6F40EE 0.5:#DBD0FB 1:#fff").style({
      fillOpacity: 0.2,
    });

    chart.render();


}




exports.handler = (req, resp, context) => {
    console.log('hello world');


    let query = req.queries
    drawPie(query);
    resp.setStatusCode(200)
    resp.setHeader('content-type', 'image/png')

    // let filename = md5(query.data)
    try{
      resp.send(canvas
          .createPNGStream())
    }catch(e){
      console.log(e)
    }

        


}